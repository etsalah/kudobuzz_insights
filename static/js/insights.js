$(function(){

//    $('#coupon').couponWidget({});

    var remove_active = function(variable){
        var oneDiv = $('#one'),
            twoDiv = $('#two'),
            threeDiv = $('#three');

        oneDiv.empty();
        twoDiv.empty();
        threeDiv.empty();

        $('.main-nav li').removeClass("main-nav--active");
        variable.parent().addClass("main-nav--active");

    };


    $('body').on('click', '.account-widget', function(event) {
        event.preventDefault();
        remove_active();
        console.log($(this).attr('id'));
        self.location = (
            self.location.origin + '?account_id=' + $(this).attr('id'));
    });


    $('body').on('click', '#account', function(event) {
        event.preventDefault();
        remove_active($(this));
        $.ajax({
            url: (
                '/api/v1/account?reviews_greater_than=1000'),
            type: 'GET',
            success: function(data) {
                createWidgets(data, 'accounts', generateAccountWidget);
            }
        });
    });


    $('body').on('click', '#userMostOrders', function(event){
        event.preventDefault();
        remove_active($(this));

        $.ajax({
            url: (
                '/api/v1/order/most/' + getUrlParameter('account_id') +
                '?limit=10'),
            type: 'GET',
            success: function(data){
                createWidgets(data, 'customers', generateReviewerCustomers);
            }

        });
    });

    $('body').on('click', '#unsatisfied', function(event){
        event.preventDefault();
        remove_active($(this));

        $.ajax({
            url: (
                '/api/v1/reviewer/worst/' + getUrlParameter('account_id') +
                '?limit=10'),
            type: 'GET',
            success: function(data){
                createWidgets(data, 'reviewers', generateReviewerWidget);
            }

        });
    });

    $('body').on('click', '#popularProducts', function(event){
        event.preventDefault();
        remove_active($(this));

        $.ajax({
            url: (
                '/api/v1/product/review/best/' + getUrlParameter('account_id') +
                '?limit=10'),
            type: 'GET',
            success: function(data){
                createWidgets(data, 'product_reviews', generateProductWidget);
            }

        });
    });

    $('body').on('click', '#unpopularProducts', function(event){
        event.preventDefault();
        remove_active($(this));

        $.ajax({
            url: (
                '/api/v1/product/review/worst/' + getUrlParameter(
                    'account_id') + '?limit=10'),
            type: 'GET',
            success: function(data){
                createWidgets(data, 'product_reviews', generateProductWidget);
            }
        });
    });

    var ravingFuns = function(){
        remove_active($('#ravingFuns'));
        $.ajax({
            url: (
                '/api/v1/reviewer/best/' + getUrlParameter('account_id') +
                '?limit=10'),
            type: 'GET',
            'success': function(data){
                createWidgets(data, 'reviewers', generateReviewerWidget);
            }
         });
    }

    ravingFuns();

    $('body').on('click', '#ravingFuns', function(event){
        event.preventDefault();
        ravingFuns();
    });


    var createWidgets = function(data, dataKey, widgetGeneratorFunc){
        var oneDiv = $('#one'),
            twoDiv = $('#two'),
            threeDiv = $('#three');

        if (data[dataKey]) {
            $.each(data[dataKey], function(index, value){
                if ([0, 1, 2].indexOf(index) >= 0){
                    oneDiv.append(widgetGeneratorFunc(value));
                }else if([3, 4, 5].indexOf(index) >= 0) {
                    twoDiv.append(widgetGeneratorFunc(value));
                }else if([6, 7, 8].indexOf(index) >= 0) {
                    threeDiv.append(widgetGeneratorFunc(value));
                }
            });

        }
    };

    var generateReviewerWidget = function(doc){

        var starString = "",
            msg = (
                "Average of " + doc['average'] + " star(s) out of " +
                doc['count'] + " review(s)"),
            picHeight = '95px', picWidth = '90px';

        for (var i = 0, k = doc['average']; i < k; i++){
            starString += "<i class='pe-7f-star'></i>";
        }

        var stringTemplate = (
            "<article class='widget' id='" + doc['_id'] + "'>" +
                "<div class='widget__content'>" +
				    "<div class='clearfix'></div>" +
                    "<div class='members__container'>" +
                        "<div class='media message checked'>" +
                            "<figure class='pull-left rounded-image message__img'>" +
                                "<object data='" + doc['pic'] + "' type='image/png' width='" + picWidth + "' height='" + picHeight + "'>" +
                                    "<img src='/static/images/avatar.png' with='" + picWidth + "' height='" + picHeight + "'/>" +
                                '</object>' +
                            "</figure>" +
                            "<div class='media-body'>" +
                                "<h4 class='media-heading message__heading'>" +  doc['display_name'] + "</h4>" +
                                "<p class='message__location'>" + starString + "</p>" +
                                "<input type='checkbox' class='btn-more-check' id='more2' checked=''>" +
                                "<div class='message__details'>" +
                                    "<table><tbody><tr>" +
                                        "<td>" +
                                            "<p class='message__msg'>" + msg + "</p>" +
                                        "</td></tr>" +
                                    "</tbody></table>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</div> <!-- /members__container -->" +
                    "<div class='clearfix'></div>" +
                    "<div class='members__footer'>" +
                        "<button class='members__load-more'>" +
                            "<span> </span> " +
                        "</button>" +
                        "<button class='members__search'>" +
                            "<i class='pe-7s-graph2'></i> Add Coupon" +
                        "</button>" +
                    "</div>" +
                "</div>" +
            "</article>");

        return stringTemplate;
    };

    var generateProductWidget = function(doc){
        var starString = "",
            msg = (
                "Average of " + doc['average'] + " star(s) out of " +
                doc['count'] + " review(s)"),
            picHeight = '95px', picWidth = '90px';

        for (var i = 0, k = doc['average']; i < k; i++){
            starString += "<i class='pe-7f-star'></i>";
        }

        var stringTemplate = (
            "<article class='widget' id='" + doc['_id'] + "'>" +
                "<div class='widget__content'>" +
				    "<div class='clearfix'></div>" +
                    "<div class='members__container'>" +
                        "<div class='media message checked'>" +
                            "<figure class='pull-left rounded-image message__img'>" +
                                "<object data='" + doc['image_url'] + "' type='image/png' width='" + picWidth + "' height='" + picHeight + "'>" +
                                    "<img src='/static/images/product-placeholder.png' with='" + picWidth + "' height='" + picHeight + "'/>" +
                                '</object>' +
                            "</figure>" +
                            "<div class='media-body'>" +
                                "<h4 class='media-heading message__heading'>" +  (doc['name'] ? doc['name'] : (doc['display_name']) ? doc['display_name'] : 'No name or handle') + "</h4>" +
                                "<p class='message__location'>" + starString + "</p>" +
                                "<input type='checkbox' class='btn-more-check' id='more2' checked=''>" +
                                "<div class='message__details'>" +
                                    "<table><tbody><tr>" +
                                        "<td>" +
                                            "<p class='message__msg'>" + msg + "</p>" +
                                        "</td></tr>" +
                                    "</tbody></table>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</div> <!-- /members__container -->" +
                    "<div class='clearfix'></div>" +
                    "<div class='members__footer'>" +
                        "<button class='members__load-more'>" +
                            "<span> </span> " +
                        "</button>" +
                        "<button class='members__search'>" +
                            "<i class='pe-7s-graph2'></i> Add Coupon" +
                        "</button>" +
                    "</div>" +
                "</div>" +
            "</article>");

        return stringTemplate;
    };

    var generateReviewerCustomers = function(doc){

        var starString = "",
            msg = "Number of orders " + doc['count'] ;

        var stringTemplate = (
            "<article class='widget' id='" + doc['email'] + "'>" +
                "<div class='widget__content'>" +
				    "<div class='clearfix'></div>" +
                    "<div class='members__container'>" +
                        "<div class='media message checked'>" +
                            "<div class='media-body'>" +
                                (doc['email'] ? doc['email'] : 'No email provided') +
                                "<p class='message__location'>" + starString + "</p>" +
                                "<input type='checkbox' class='btn-more-check' id='more2' checked=''/>" +
                                "<div class='message__details'>" +
                                    "<table><tbody><tr>" +
                                        "<td>" +
                                            "<p class='message__msg'>" + msg + "</p>" +
                                        "</td></tr>" +
                                    "</tbody></table>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</div> <!-- /members__container -->" +
                "</div>" +
            "</article>");

        return stringTemplate;
    };


    var generateAccountWidget = function(doc) {
        var starString = "",
            msg = "Number of reviews " + doc['review_count'] ;


        var stringTemplate = (
            "<article class='widget account-widget' id='" + doc['account_id'] + "'>" +
                "<div class='widget__content'>" +
				    "<div class='clearfix'></div>" +
                    "<div class='members__container'>" +
                        "<div class='media message checked'>" +

                            "<div class='media-body'>" +
                                "<p>Account Id: " +  doc['account_id'] + "</p>" +
                                "<input type='checkbox' class='btn-more-check' id='more2' checked=''>" +
                                "<div class='message__details'>" +
                                    "<table><tbody><tr>" +
                                        "<td>" +
                                            "<p class='message__msg'>" + msg + "</p>" +
                                        "</td></tr>" +
                                    "</tbody></table>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</div> <!-- /members__container -->" +
                    "<div class='clearfix'></div>" +
                "</div>" +
            "</article>");

        return stringTemplate;
    };

});