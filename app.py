from flask import Flask
from flask import jsonify
from flask import request
from bson import ObjectId
from models.db import get_db
from models.account import Account
from models.product import Product
from models.product_review import ProductReview
from models.reviewer import Reviewer
from models.order import Order
from flask import render_template


app = Flask(__name__)
conn, operational_db = get_db('PRODUCTION')
Account.db = operational_db
Product.db = operational_db
ProductReview.db = operational_db
Reviewer.db = operational_db
Order.db = operational_db


@app.route('/', methods=['GET',])
def raving_funs():
    return render_template('dashboard.html')


@app.route('/api/v1/account', methods=['GET', ])
def get_accounts():
    reviews_greater_than = int(request.args.get('reviews_greater_than', 100))
    return jsonify({
        'accounts':
            Account.get_account_with_product_reviews(
                int(reviews_greater_than))})


@app.route('/api/v1/product/<int:account_id>', methods=['GET', ])
@app.route('/api/v1/product/<int:account_id>/<object_id>', methods=['GET', ])
@app.route(
    '/api/v1/product/<int:account_id>/<object_id>/<direction>',
    methods=['GET', ])
def get_products(account_id, object_id=None, direction='forward'):
    args = {'account_id': account_id}

    if object_id and direction in ('forward', 'back', 'backward'):
        if direction == 'forward':
            direction = '$gt'
        elif direction in ('back', 'backward'):
            direction = '$lt'
        args['_id'] = {direction: ObjectId(object_id)}
    limit = request.args.get('limit', 100)
    return jsonify({'products': Product.list(args, limit)})


@app.route(
    '/api/v1/product/review/<review_type>/<int:account_id>', methods=['GET', ])
@app.route(
    '/api/v1/product/review/<review_type>/<int:account_id>/<product_id>',
    methods=['GET', ])
def get_product_reviews(
        review_type, account_id, product_id=None):

    limit = int(request.args.get('limit', 100))
    results = []

    count = 5
    if review_type == 'worst':
        average = 3
    else:
        average = 4

    for product_review in ProductReview.get_best_matched_reviews(
            review_type, account_id, product_id, limit, count, average):
        product = Product.find({'id': product_review['product_id']})

        if not product:

            product = Product.get_dummy_product()
            product['account_id'] = account_id

        elif not product['image_url']:
            product['image_url'] = 'http://www.example.com/dummy_product.png'

        product['average'] = int(product_review['average'])
        product['count'] = product_review['count']

        results.append(product)

    return jsonify({'product_reviews': results})


@app.route(
    '/api/v1/reviewer/<reviewer_type>/<int:account_id>', methods=['GET', ])
@app.route(
    '/api/v1/reviewer/<reviewer_type>/<int:account_id>/<product_id>',
    methods=['GET', ])
def get_best_reviewer(reviewer_type, account_id, product_id=None):
    limit = int(request.args.get('limit', 100))
    results = []

    for result in ProductReview.get_best_matched_reviewers(
            reviewer_type, account_id, product_id, limit):
        reviewer = Reviewer.find({'id': result['reviewer']})

        if not reviewer:
            reviewer = Reviewer.get_dummy_reviewer()

        reviewer.update({
            'count': result['count'],
            'average': int(result['average_rating'])})

        results.append(reviewer)

    return jsonify({'reviewers': results})


@app.route('/api/v1/order/most/<int:account_id>', methods=['GET', ])
@app.route(
    '/api/v1/order/most/<int:account_id>/<product_id>', methods=['GET', ])
def get_customer_with_most_orders(account_id, product_id=None):
    limit = int(request.args.get('limit', 100))
    return jsonify({
        'customers': Order.get_customer_with_most_orders(
            account_id, product_id, limit)})


@app.route('/api/v1/reviewer/<reviewer_id>', methods=['GET', ])
def get_reviewer_details(reviewer_id):
    return jsonify({'reviewer': Reviewer.find({'id': reviewer_id})})


if __name__ == '__main__':
    app.run(debug=True, port=8080, host='0.0.0.0')
