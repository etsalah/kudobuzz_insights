#!/usr/bin/env python3
from models.model_util import return_dict
from uuid import uuid1


class Product:
    COLLECTION_NAME = 'product'
    COLUMNS = ('_id', 'name', 'handle', 'account_id', 'id', 'image_url')
    db = None

    def __init__(self):
        for column in Product.COLUMNS:
            setattr(self, column, None)

    def dict(self):
        return return_dict(self, Product.COLUMNS)

    @staticmethod
    def list(args, limit=100):
        collection = Product.db[Product.COLLECTION_NAME]
        cursor = collection.find(args).limit(limit)
        results = []
        for row in cursor:
            obj = Product()
            for column in row:
                setattr(obj, column, row[column])

            results.append(obj.dict())

        return results

    @staticmethod
    def find(args):
        results = Product.list(args, 1)

        if results:
            return results[0]

    @staticmethod
    def get_dummy_product():
        _id = str(uuid1())

        return {
            '_id': _id, 'id': _id, 'name': "Product wasn't found",
            'handle': "Product wasn't found", 'account_id': '',
            'image_url': 'http://www.example.com/dummy_product.png'}
