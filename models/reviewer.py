#!/usr/bin/env python3
from uuid import uuid1
from models.model_util import return_dict
from datetime import datetime


class Reviewer:
    COLLECTION_NAME = 'reviewer'
    COLUMNS = (
        '_id', 'id', 'display_name', 'verification_email_sent', 'pic',
        'created_on', 'verified', 'user_name', 'email', 'channel')
    db = None

    def __init__(self):
        for column in Reviewer.COLUMNS:
            setattr(self, column, None)

    def dict(self):
        return return_dict(self, Reviewer.COLUMNS)

    @staticmethod
    def list(args, limit=100):
        collection = Reviewer.db[Reviewer.COLLECTION_NAME]
        results = []

        for row in collection.find(args).sort([('_id', 1), ]).limit(limit):
            obj = Reviewer()
            for column in row:
                setattr(obj, column, row[column])

            results.append(obj.dict())
        return results

    @staticmethod
    def find(args):
        results = Reviewer.list(args, limit=1)

        if results:
            return results[0]

    @staticmethod
    def get_dummy_reviewer():
        _id = str(uuid1())
        return {
            '_id': _id, 'id': _id, 'display_name': 'Dummy Reviewer',
            'verification_email_sent': False,
            'pic': 'http://www.example.com/reviewer.png',
            'created_on': datetime.today(), 'verified': False,
            'user_name': 'Dummy Reviewer',
            'channel': 'email', 'email': 'dummy@example.com'}