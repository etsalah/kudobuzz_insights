#!/usr/bin/env python3
import os
from urllib import parse
from pymongo import MongoClient


CONFIG = {
    'DEVELOPMENT': {
        'OPERATIONAL_DB': {
            'DB_URL': '127.0.0.1', 'DB_PORT': 27017,
            'DATABASE': 'kudobuzz', 'USERNAME': 'kudomin',
            'PASSWORD': 'Support101', 'AUTHENTICATE': False},
        'ACTIVITY_DB': {
            'DB_URL': '104.239.233.100', 'DB_PORT': 27017,
            'DATABASE': 'kudo_log',
            'USERNAME': 'kudomin', 'PASSWORD': 'Support101'},
        'BATCH_SIZE': 5},
    'PRODUCTION': {
        'OPERATIONAL_DB': {
            'DB_URL': '104.239.233.100', 'DB_PORT': 27017,
            'DATABASE': 'kudobuzz', 'USERNAME': 'kudomin',
            'PASSWORD': 'Support101', 'AUTHENTICATE': True},
        'ACTIVITY_DB': {
            'DB_URL': '104.239.233.100', 'DB_PORT': 27017,
            'DATABASE': 'kudobuzz_log',
            'USERNAME': 'kudomin', 'PASSWORD': 'Support101'},
        'BATCH_SIZE': 100}}

PYTHON_ENV = os.environ.get('PYTHON_ENV', None)

if PYTHON_ENV not in ('DEVELOPMENT', 'TEST'):
    CONFIG_KEY = 'PRODUCTION'
elif PYTHON_ENV == 'DEVELOPMENT':
    CONFIG_KEY = 'DEVELOPMENT'
else:
    CONFIG_KEY = 'TEST'


def get_db(config_key=CONFIG_KEY, which_db='OPERATIONAL_DB'):

    connection = MongoClient(
        CONFIG[config_key][which_db]['DB_URL'],
        CONFIG[config_key][which_db]['DB_PORT'], j=True)

    db = connection[CONFIG[config_key][which_db]['DATABASE']]

    if CONFIG[config_key][which_db]['AUTHENTICATE']:
        db.authenticate(
            CONFIG[config_key][which_db]['USERNAME'],
            parse.quote_plus(CONFIG[config_key][which_db]['PASSWORD']),
            mechanism='SCRAM-SHA-1')

    return connection, db
