#!/usr/bin/env python3


def return_dict(obj, attributes):
    tmp = {}
    for attribute in attributes:
        value = getattr(obj, attribute, None)
        if attribute == '_id' and value:
            value = str(value)
        tmp[attribute] = value

    return tmp
