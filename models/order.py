#!/usr/bin/env python3
from models.model_util import return_dict


class Order:
    COLLECTION_NAME = 'order'
    COLUMNS = ('_id', 'name', 'handle', 'account_id', 'id', 'image_url')
    db = None

    def __init__(self):
        for column in Order.COLUMNS:
            setattr(self, column, None)

    def dict(self):
        return return_dict(self, Order.COLUMNS)

    @staticmethod
    def list(args, limit=100):
        collection = Order.db[Order.COLLECTION_NAME]
        cursor = collection.find(args).limit(limit)
        results = []
        for row in cursor:
            obj = Order()
            for column in row:
                setattr(obj, column, row[column])

            results.append(obj.dict())

        return results

    @staticmethod
    def find(args):
        results = Order.list(args, 1)

        if results:
            return results[0]

    @staticmethod
    def get_customer_with_most_orders(account_id, product_id=None, limit=100):
        collection = Order.db[Order.COLLECTION_NAME]
        results = []

        tmp_args = {'account_id': account_id}
        if product_id:
            tmp_args['product_id'] = product_id

        args = [
            {'$match': tmp_args},
            {
                '$group': {
                    '_id': {
                        'account_id': '$account_id', 'shop_id': '$shop_id',
                        'email': '$customer.email'}, 'count':{'$sum':1}}},
            {'$sort': {'count': -1}},
            {'$limit': limit}
        ]

        for row in collection.aggregate(args):
            tmp_data = row['_id']

            results.append({
                'email': tmp_data.get('email', ''), 'count': row['count']})

        return results
