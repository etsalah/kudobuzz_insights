#!/usr/bin/env python3
from models.model_util import return_dict


class ProductReview:
    COLLECTION_NAME = 'product_review'
    COLUMNS = (
        '_id', 'id', 'rating', 'is_after_purchase', 'account_id', 'title',
        'created_at', 'published', 'reviewer', 'message', 'product_id')
    db = None

    def __init__(self):
        for column in ProductReview.COLUMNS:
            setattr(self, column, None)

    def dict(self):
        return return_dict(self, ProductReview.COLUMNS)

    @staticmethod
    def list(args, limit=100):
        collection = ProductReview.db[ProductReview.COLLECTION_NAME]
        results = []
        for row in collection.find(args).sort([('_id', 1), ]).limit(limit):
            obj = ProductReview()
            for column in row:
                setattr(obj, column, row[column])

            results.append(obj.dict())
        return results

    @staticmethod
    def get_best_matched_reviews(
            review_type, account_id, product_id=None, limit=100,
            count=5, average=4):
        collection = ProductReview.db[ProductReview.COLLECTION_NAME]
        results = []

        if review_type == 'worst':
            sort_direction = 1
            average_stage = {'$match': {'average': {'$lte': average}}}
        else:
            sort_direction = -1
            average_stage = {'$match': {'average': {'$gte': average}}}

        tmp_args = {'account_id': account_id}

        if product_id:
            tmp_args['product_id'] = product_id

        args = [
            {'$match': tmp_args},
            {'$group': {
                '_id': {
                    'product_id': "$product_id", 'account_id': '$account_id'},
                'count': {'$sum': 1},
                'average': {'$avg': "$rating"}}},
            {'$match': {'count': {'$gte': count}}},
            average_stage,
            {'$sort': {'average': sort_direction, 'count': sort_direction}},
            {'$limit': limit}]

        for row in collection.aggregate(args):
            tmp_data = row['_id']

            results.append({
                'product_id': tmp_data['product_id'], 'count': row['count'],
                'average': row['average']})

        return results

    @staticmethod
    def get_best_matched_reviewers(
            reviewer_type, account_id, product_id=None, limit=100):
        collection = ProductReview.db[ProductReview.COLLECTION_NAME]
        results = []

        if reviewer_type == 'worst':
            sort_direction = 1
            average_segment = {'$match': {'average': {'$lte': 3}}}
            count_segment = {'$match': {'count': {'$gte': 2}}}
        else:
            sort_direction = -1
            average_segment = {'$match': {'average': {'$gte': 4}}}
            count_segment = {'$match': {'count': {'$gte': 5}}}

        tmp_args = {'account_id': account_id}

        if product_id:
            tmp_args['product_id'] = product_id

        args = [
            {'$match': tmp_args},
            {'$group': {
                '_id': {'reviewer': '$reviewer', 'account_id': '$account_id'},
                'count': {'$sum': 1}, 'average': {'$avg': '$rating'}}},
            count_segment, average_segment,
            {'$sort': {'average': sort_direction, 'count': sort_direction}},
            {'$limit': limit}]

        for row in collection.aggregate(args):
            group_data = row['_id']

            results.append({
                'reviewer': group_data['reviewer'], 'count': row['count'],
                'average_rating': row['average']})

        return results
