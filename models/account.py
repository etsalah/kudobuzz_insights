#!/usr/bin/env python3
from models.product_review import ProductReview


class Account:
    COLLECTION_NAME = 'account'
    COLUMNS = ()
    db = None

    def __init__(self):
        for column in Account.COLUMNS:
            setattr(self, column, None)

    @staticmethod
    def list(args, limit):
        pass

    @staticmethod
    def find(account_id):
        pass

    @staticmethod
    def get_account_with_product_reviews(greater_than_count=0):
        collection = Account.db[Account.COLLECTION_NAME]
        product_collection = Account.db[ProductReview.COLLECTION_NAME]
        cursor = product_collection.aggregate([
            {'$group': {'_id': "$account_id", 'total': {'$sum': 1}}},
            {'$sort': {'total': -1}},
            {'$match': {'total': {'$gt': greater_than_count}}}])

        results = []
        for row in cursor:
            # if row['total'] >= greater_than_count:
            results.append(
                {'account_id': row['_id'], 'review_count': row['total']})

        return results